use crate::sprite::Sprite;

pub struct Blast {
    pub pos: (i32, i32),
    pub spr: Sprite,
}
impl Blast {
    pub fn new(p: (i32, i32)) -> Blast{
        let b = Blast {
            pos: p,
            spr: Sprite::from((0,8),(8,8),(8,8)),
        };
        b
    }
}

pub struct Player {
    pub pos: (i32, i32),
    pub spr: Sprite,
    pub life: u8,
    pub score: u8,
    pub blasts: Vec<Blast>,
}

impl Player {
    pub fn move_right(&mut self) {
        self.pos.0 += 8;
    }
    pub fn shoot(&mut self){
        self.blasts.push(Blast::new(self.pos));
    }
    pub fn move_left(&mut self) {
        self.pos.0 -= 8;
    }
}
