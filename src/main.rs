extern crate sdl2;

use sdl2::event::Event;
use sdl2::keyboard::Keycode;
use sdl2::rect::Point;
use std::collections::HashSet;
use std::path::Path;
use std::time::Duration;
mod player;
mod sprite;

const WEIGHT: u32 = 800;
const HEIGHT: u32 = 600;

fn main() {
    let sdl_context = sdl2::init().unwrap();
    let video_subsystem = sdl_context.video().unwrap();
    let window = video_subsystem
        .window("oceaninvaders", WEIGHT, HEIGHT)
        .build()
        .map_err(|e| e.to_string())
        .unwrap();

    let mut canvas = window
        .into_canvas()
        .accelerated()
        .build()
        .map_err(|e| e.to_string())
        .unwrap();
    let texture_creator = canvas.texture_creator();
    let temp_surface = sdl2::surface::Surface::load_bmp(Path::new("resources/tiles.bmp")).unwrap();
    let texture = texture_creator
        .create_texture_from_surface(&temp_surface)
        .map_err(|e| e.to_string())
        .unwrap();

    let mut bullets: Vec<player::Blast> = Vec::new();
    let mut player = player::Player {
        pos: (16, 560),
        spr: sprite::Sprite::from((0, 0), (8, 8), (8, 8)),
        life: 255,
        score: 0,
        blasts: bullets,
    };

    let mut event_pump = sdl_context.event_pump().unwrap();
    canvas.set_draw_color(sdl2::pixels::Color::RGBA(0, 255, 255, 255));
    'run: loop {
        for event in event_pump.poll_iter() {
            match event {
                Event::Quit { .. }
                | Event::KeyDown {
                    keycode: Some(Keycode::Escape),
                    ..
                } => break 'run,
                _ => {}
            }
        }
        canvas.clear();

        let keys: HashSet<Keycode> = event_pump
            .keyboard_state()
            .pressed_scancodes()
            .filter_map(Keycode::from_scancode)
            .collect();
        for i in keys.iter() {
            match i {
                Keycode::Left => player.move_left(),
                Keycode::Right => player.move_right(),
                Keycode::Space => player.shoot(),
                _ => {}
            }
        }
        for i in player.blasts.iter_mut() {
            i.spr.dest_rect.center_on(Point::new(i.pos.0, i.pos.1));
            canvas
                .copy_ex(
                    &texture,
                    Some(i.spr.source_rect),
                    Some(i.spr.dest_rect),
                    0.0,
                    None,
                    false,
                    false,
                )
                .unwrap();
            i.pos.1 -= 32;
        }
        if player.blasts.len() > 0 {
            player.blasts.retain(|x| x.pos.1 > 0);
        }
        player
            .spr
            .dest_rect
            .center_on(Point::new(player.pos.0, player.pos.1));

        canvas
            .copy_ex(
                &texture,
                Some(player.spr.source_rect),
                Some(player.spr.dest_rect),
                0.0,
                None,
                false,
                false,
            )
            .unwrap();
        canvas.present();
        std::thread::sleep(Duration::from_millis(100));
    }
}
