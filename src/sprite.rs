use sdl2::rect::Rect;

pub struct Sprite {
    pub size: (u32, u32),
    pub source_rect: Rect,
    pub dest_rect: Rect,
}

impl Sprite {
    pub fn from(p: (i32, i32), s: (u32, u32), w: (u32, u32)) -> Sprite{
        let spr = Sprite {
            size: s,
            source_rect: Rect::new(p.0,p.1, s.0, s.1),
            dest_rect: Rect::new(0,0, s.0 * w.0, s.0 * w.0)
        };
        spr
    }
}
